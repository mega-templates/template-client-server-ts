import { plainToInstance } from 'class-transformer';
import { ENVTypes } from './constants';

class ProcessENV {
  public CUSTOM_ENV: ENVTypes = ENVTypes.DEV;

  //Порты для запуска самого сервера
  public SERVER_PORT: number = 3000;

  //URL
  public SERVER_URL: string = '';

  //Всё что касается основной БД
  public DB_URL: string = 'localhost';
  public DB_PORT: number = 5432;
  public DB_NAME: string = 'dev_example_db';
  public DB_USERNAME: string = '';
  public DB_PASSWORD: string = '';

  //Все что касается редиски
  public REDIS_DB_URL: string = 'IP';
  public REDIS_PORT: number = 6379;
  public REDIS_DB_NUMBER: number = 0;
  public REDIS_PASSWORD: string = '';
}

export default class UtilsENVConfig {
  private static processENV: ProcessENV | null = null;

  static getProcessEnv(): ProcessENV {
    if (this.processENV === null) {
      this.processENV = plainToInstance(ProcessENV, process.env);
    }
    return this.processENV;
  }

  static isAvailable(...availableTypes: ENVTypes[]): boolean {
    return availableTypes.includes(this.getProcessEnv().CUSTOM_ENV);
  }
}
